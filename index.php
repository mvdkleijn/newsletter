<?php

/**
 * Easy Newsletter plugin for Wolf CMS
 * 
 * Based on ideas and code snippets by Laszlo Sebetyen Horvath's Simple Newsletter.
 * 
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2012
 * @license GPL3
 */

include('lib/class.phpmailer.php');

Plugin::setInfos(array(
    'id' => 'newsletter',
    'title' => 'Easy Newsletter',
    'description' => 'Easy newsletter',
    'version' => '0.51',
    'type'  => 'both',
    'author' => 'Martijn van der Kleijn',
    'website' => 'http://vanderkleijn.net'
));

if (Plugin::isEnabled('newsletter')) {
    // Setup the controller.
    Plugin::addController("newsletter", __('Newsletter'), 'admin_view', true);

    // Load models
    AutoLoader::addFolder(PLUGINS_ROOT . '/newsletter/models');

    // Setup routes to the forum.
    Dispatcher::addRoute(array(
        '/newsletter/unsubscribe'  => '/plugin/newsletter/unsubscribe',
        '/newsletter/subscribe'    => '/plugin/newsletter/subscribe'
        ));
    
    function subscribe($groupname=false) {
        if ($groupname === false) echo __('Our apologies, the newsletter system is not yet available.');
        
        echo '<form class="simple_form" method="post" action=' . BASE_URL . 'newsletter/subscribe'.URL_SUFFIX.'>'."\n";
        echo '    <fieldset>'."\n";
        echo '        <h4>' . __('Newsletter subscription') . '</h4>'."\n";
        echo '        <input type="hidden" value="'.$groupname.'" name="group" />'."\n";
        echo '        <label>' . __('Name') . '</label> <input type="text" name="name" class="text" /><br />'."\n";
        echo '        <label>' . __('Email') . '</label> <input type="text" name="email" class="text" /><br />'."\n";
        echo '        <input type="submit" value=' . __('Subscribe') . ' name="subscribe" class="submit" />'."\n";
        echo '    </fieldset>'."\n";
        echo '</form>'."\n";
    }

}
