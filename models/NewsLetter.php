<?php

/**
 * Easy Newsletter plugin for Wolf CMS
 * 
 * Based on ideas and code snippets by Laszlo Sebetyen Horvath's Simple Newsletter.
 * 
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2012
 * @license GPL3
 */
class NewsLetter extends Record {

    const TABLE_NAME = 'news_letter';

    public $id;
    public $title = '';
    public $subject = '';
    public $content = '';

}