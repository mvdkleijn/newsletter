<?php

/**
 * Easy Newsletter plugin for Wolf CMS
 * 
 * Based on ideas and code snippets by Laszlo Sebetyen Horvath's Simple Newsletter.
 * 
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2012
 * @license GPL3
 */
class NewsUser extends Record {

    const TABLE_NAME = 'news_user';

    public $id;
    public $name = '';
    public $email = '';
    public $subscribed;
    public $hash = '';

    public static function findGroups() {
        return NewsUserGroups::findAllFrom('NewsUserGroups', "user_id=?", array($this->id));
    }
    
    public static function findByHash($hash) {
        return self::findOneFrom('NewsUser', 'hash=?', array($hash));
    }
}