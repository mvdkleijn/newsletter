<?php

/**
 * Easy Newsletter plugin for Wolf CMS
 * 
 * Based on ideas and code snippets by Laszlo Sebetyen Horvath's Simple Newsletter.
 * 
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2012
 * @license GPL3
 */
class NewsGroup extends Record {

    const TABLE_NAME = 'news_group';

    public $id;
    public $name = '';

    public static function findUsers($gid) {
        return NewsUserGroups::findAllFrom('NewsUserGroups', "group_id=?", array($gid));
    }
}