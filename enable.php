<?php

/**
 * Easy Newsletter plugin for Wolf CMS
 * 
 * Based on ideas and code snippets by Laszlo Sebetyen Horvath's Simple Newsletter.
 * 
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2012
 * @license GPL3
 */

// security feature
if (!defined('IN_CMS')) exit;

$driver = Record::getConnection()->getAttribute(PDO::ATTR_DRIVER_NAME);

if (Record::query('select * from news_users limit 1') !== false) {
    
} else {
    if ($driver == 'mysql') {
        $sql = 'CREATE TABLE `'.TABLE_PREFIX.'news_group` (
                `id` int(11) NOT NULL auto_increment,
                `name` varchar(255) NOT NULL,
                PRIMARY KEY  (`id`)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8';    
        if (Record::query($sql) === false) {
            Flash::set('error', __('Unable to create :tablename table.', array(':tablename' => 'news_groups')));
            return;
        }

        $sql = 'CREATE TABLE `'.TABLE_PREFIX.'news_letter` (
                `id` int(11) NOT NULL auto_increment,
                `title` varchar(255) NOT NULL,
                `subject` varchar(255) NOT NULL,
                `content` longtext NOT NULL,
                PRIMARY KEY  (`id`)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8';
        if (Record::query($sql) === false) {
            Flash::set('error', __('Unable to create :tablename table.', array(':tablename' => 'news_letters')));
            return;
        }
        
        $sql = 'CREATE TABLE `'.TABLE_PREFIX.'news_user` (
                `id` int(11) NOT NULL auto_increment,
                `name` varchar(255) NOT NULL,
                `email` varchar(255) NOT NULL,
                `subscribed` tinyint(1) NOT NULL,
                `hash` varchar(64) NOT NULL,
                PRIMARY KEY  (`id`)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8';
        if (Record::query($sql) === false) {
            Flash::set('error', __('Unable to create :tablename table.', array(':tablename' => 'news_users')));
            return;
        }
        
        $sql = 'CREATE TABLE `'.TABLE_PREFIX.'news_users_groups` (
                `id` int(11) NOT NULL auto_increment,
                `user_id` int(11) NOT NULL,
                `group_id` int(11) NOT NULL,
                PRIMARY KEY  (`id`),
                UNIQUE KEY id (user_id,group_id)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8';
        if (Record::query($sql) === false) {
            Flash::set('error', __('Unable to create :tablename table.', array(':tablename' => 'news_users_groups')));
            return;
        }
    }
}
