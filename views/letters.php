<h1><?php echo __('Letter Management'); ?></h1>
<div id="letters">

    <h2><?php echo __('Existing letters'); ?></h2>
    <div class='letters'>
        <?php foreach ($letters as $letter) { ?>
            <div onclick="changeStyle('<?php echo $letter->id; ?>')" style="padding:4px;font-weight:bold;cursor:pointer;">
                <img src="<?php echo PLUGINS_URI; ?>newsletter/icons/plus.png" align="top" alt="" /> <?php echo $letter->title; ?></div>
            <div style="border:1px solid #c3c3c3;">
                <div id="<?php echo $letter->id; ?>" style="display:none;">
                    <div style="text-decoration:underline;padding:4px 0;"><?php echo __('Subject') . ': ' . $letter->subject; ?></div>
                    <div style="padding:4px;"><?php echo $letter->content; ?></div>
                    <div><form action="<?php echo get_url('plugin/newsletter/letter/change'); ?>" method="post">
                            <input type="hidden" name="id" value="<?php echo $letter->id; ?>">
                            <input type="submit" name="mod" value="<?php echo __('Modify'); ?>">
                            <input type="submit" name="del" value="<?php echo __('Delete'); ?>">
                        </form></div>
                </div></div>
        <?php } ?>

    </div>

</div>

<h2><?php echo __('Add a new letter'); ?></h2>
<div id="new-letter">
    <form action="<?php echo get_url('plugin/newsletter/letters/add'); ?>" name="" method="post">
        <table class="new-letter">
            <tr>
                <td class="label"><label for="title"><?php echo __('Title'); ?></label></td>
                <td class="text"><input type="text" name="title" id="title" /></td>
            </tr>
            <tr>
                <td class="label"><label for="subject"><?php echo __('Subject'); ?></label></td>
                <td class="text"><input type="text" name="subject" id="subject" /></td>
            </tr>
            <tr>
                <td class="label"><label for="content"><?php echo __('Content'); ?></label></td>
                <td class="text"><textarea name="content" ></textarea></td>
            </tr>
        </table>
        <div class="save"><input type="submit" name="new_letter" value="<?php echo __('Save'); ?>" /></div>
    </form>
</div>