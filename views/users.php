<h1><?php echo __('Subscribers') ?></h1>

<h2><?php echo __('Add a new subscriber'); ?></h2>

<div id="new-user">
    <form action="<?php echo get_url('plugin/newsletter/users/add'); ?>" method="post" onsubmit="return check();" name="new">
        <table class="index" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td><label for="name"><?php echo __('Name') ?></label></td>
                <td><input type="text" name="name" /></td>
                <td><label for="email"><?php echo __('Email') ?></label></td>
                <td><input type="text" name="email" /></td>
            </tr>
            <tr>
                <td><label for="group"><?php echo __('Groups') ?></label></td>
                <td>
                    <?php if(empty($groups)) {
                        echo __('No groups in system.');
                    } else { ?>
                    <select name="group[]" multiple="multiple" size="5">
                        <?php foreach ($groups as $group) { ?>
                            <option value="<?php echo $group->id; ?>"><?php echo $group->name; ?></option>
                        <?php } ?>
                    </select>
                    <?php } ?>
                </td>
                <td><label for="name">&nbsp;</label></td>
                <td><input type="submit" name="add_user" value="<?php echo __('Add user'); ?>"/></td>
            </tr>
        </table>
    </form>
</div>


<div >
    <h3><?php echo __('Groups and their members'); ?></h3>
    <p>
        <?php if ($groupid === false) echo '<strong>'; ?>
        <a href="<?php echo get_url('plugin/newsletter/users/list'); ?>"><?php echo __('Subscribed');?></a> | 
        <?php if ($groupid === false) echo '</strong>'; ?>
        <?php if ($groupid === 'un') echo '<strong>'; ?>
        <a href="<?php echo get_url('plugin/newsletter/users/list/un'); ?>"><?php echo __('Unsubscribed');?></a> | 
        <?php if ($groupid === 'un') echo '</strong>'; ?>
        <?php foreach ($groups as $group) { ?>
            <?php if ($group->id === $groupid) echo '<strong>'; ?>
            <a href="<?php echo get_url('plugin/newsletter/users/list/' . $group->id); ?>"><?php echo $group->name; ?></a> | 
            <?php if ($group->id === $groupid) echo '</strong>'; ?>
        <?php } ?>
    </p>

    <div id="results">
        <table id="users" class="index" border="0" cellpadding="0" cellspacing="0">
            <form action="<?php echo get_url('plugin/newsletter/users/change'); ?>" name="" method="post">
                <?php foreach ($users as $user) { ?>
                    <tr>
                        <td><input type="checkbox" name="user_id[]" value="<?php echo $user->id; ?>" /></td>
                        <td><?php echo $user->name; ?></td>
                        <td><?php echo $user->email; ?></td>
                        <td>
                            <form action="<?php echo get_url('plugin/newsletter/users/change'); ?>" name="" method="post">
                                <?php //$this->list_groups_to_select($id); ?>
                                <select name="group[]" multiple="multiple" size="3">
                                    <?php foreach ($groups as $group) { ?>
                                        <option value="<?php echo $group->id; ?>"><?php echo $group->name; ?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" name="userid" value="<?php echo $user->id;?>"/>
                        </td>
                        <td><input type="submit" name="attach" value="<?php echo __('Change groups'); ?>" /></td>
                            </tr>                         
                <?php } ?>
                <div class="delete"><input type="submit" name="del_users" value="<?php echo __('Delete'); ?>" nclick="return confirm('Are you sure?')" /></div>
            </form>
        </table>
    </div>
</div>
<script type="text/javascript">
        function check()
        {
            if (document.new .email.value == '') {
                alert("<?php echo __('You didn\'t fill out the "Email" field'); ?>");
                return false;
            }
        }
</script>
