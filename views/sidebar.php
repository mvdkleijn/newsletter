
<p class="button"><img src="<?php echo PLUGINS_URI; ?>newsletter/icons/users.png" align="middle" alt="" /><a href="<?php echo get_url('plugin/newsletter/users');?>"><?php echo __('Subscribers');?></a></p>
<p class="button"><img src="<?php echo PLUGINS_URI; ?>newsletter/icons/compose.png" align="middle" alt="" /><a href="<?php echo get_url('plugin/newsletter/letters');?>"><?php echo __('Letter Management');?></a></p>
<p class="button"><img src="<?php echo PLUGINS_URI; ?>newsletter/icons/send.png" align="middle" alt="" /><a href="<?php echo get_url('plugin/newsletter/send');?>"><?php echo __('Send Newsletter');?></a></p>
<p class="button"><img src="<?php echo PLUGINS_URI; ?>newsletter/icons/groups.png" align="middle" alt="" /><a href="<?php echo get_url('plugin/newsletter/groups');?>"><?php echo __('Group Management');?></a></p>

<div class="box">
    <h2><?php echo __('How to use it?'); ?></h2>
    <p><?php echo __('Insert the following code where you want your subscription box to appear:'); ?></p>
    <code>
&lt;?php subscribe("your group name"); ?&gt;
</code>
</div>