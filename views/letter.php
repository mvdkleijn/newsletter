<h1><?php echo __('Modify Letter'); ?></h1>
<div id="mod_letter">
    <form action="<?php echo get_url('plugin/newsletter/letter/change'); ?>" name="" method="post">
        <table class="fieldset">
            <tr>
                <td style="width:20%"><label for="title"><?php echo __('Title'); ?></label></td>
                <td><input type="text" name="title" id="title" value="<?php echo $letter->title; ?>" /></td>
            </tr>
            <tr>
                <td style="width:20%"><label for="subject"><?php echo __('Subject'); ?></label></td>
                <td><input type="text" name="subject" id="subject" value="<?php echo $letter->subject; ?>" /></td>
            </tr>
        </table>
        <label for="content"><?php echo __('Content'); ?></label>
        <textarea name="content" ><?php echo $letter->content; ?></textarea>
        <input type="hidden" value="<?php echo $letter->id; ?>" name="id" />

        <p class="buttons"><input type="submit" name="done" value="<?php echo __('Save'); ?>" /></p>

    </form>
</div>