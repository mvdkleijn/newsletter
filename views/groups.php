<h1><?php echo __('Group Management'); ?></h1>
<h2><?php echo __('Add a group'); ?></h2>
<div class="form-area">
    <form action="<?php echo get_url('plugin/newsletter/groups/add'); ?>" method="post">
        <table class="new-group">
            <tr>
                <td><label for="name"><?php echo __('New group name'); ?></label></td>
                <td><input type="text" name="name" /></td>
                <td><input type="submit" name="add_group" value="<?php echo __('Add') ?>"/></td>
            </tr>
        </table>
    </form>
</div>

<h2><?php echo __('Delete one or more groups'); ?></h2>
<form action="<?php echo get_url('plugin/newsletter/groups/delete'); ?>"  method="post">
    <table class="index" border="0" cellpadding="0" cellspacing="0">
        <?php foreach ($groups as $group) { ?>
            <tr>
                <td class="group-checkbox">
                    <input type="checkbox" name="group[]" value="<?php echo $group->id; ?>" />
                </td>
                <td><?php echo $group->name; ?></td>
            </tr>
        <?php } ?>
    </table>
    <div class="delete"><input type="submit" name="delete_group" value="<?php echo __('Delete'); ?>"/></div>
</form>

