<h1>Easy Newsletter</h1>

<h2>About</h2>
<p>
    Members can subscribe themselves or can be added on the administrative
    interface, where you can easily group them. Letters are saved after sending
    and you can re-edit and re-send them to all of your subscribers or just to
    particular groups if you wish.
</p>

<h2>Features</h3>
<ul style="margin-left:20px; list-style-type: disc">
    <li>Add and manage your subscribers in an easy way;</li>
    <li>Create and modify groups of your subscribers;</li>
    <li>Connect users to one or more groups;</li>
    <li>Subscribe form anywhere on your web page;</li>
    <li>Direct unsubscribe link at the end of the letters;</li>
</ul>

<h2>Usage</h2>
<p>
    This plugin easy to use, unzip the file and move the newsletter folder to
    your /wolf/plugins/ directory. Go to the Administration > Plugins tab in
    your wolf CMS Admin, and enable the plugin.
</p>
<p>Next:</p>
<ul style="margin-left:20px; list-style-type: disc">
    <li>Create a group;</li>
    <li>Create a newsletter if desired;</li>
    <li>Insert the following code where you want your subscription box to appear:</li>
</ul>
    <pre><code>
    &lt;?php subscribe("your group name"); ?&gt;
    </code></pre>
<p>
    Thats all, ready to use.
</p>