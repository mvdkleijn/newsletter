<h1><?php echo __('Send letter'); ?></h1>
    <?php if (empty($groups)) { ?>
        <div class="box" style="margin:10px 0px; padding: 5px; border:1px solid red;">
            <?php echo __('Unable to send a letter, please create at least one group.');
            ?>
        </div>
    <?php } ?>

    <h3><?php echo __('Select letter'); ?></h3>
    <form name="sendForm" action="<?php echo get_url('plugin/newsletter/send/confirm'); ?>" method="post">
        <ul style="float:left;clear:both;width:100%;padding:2px 0 15px 0;">
            <?php foreach ($letters as $letter) { ?>
                <li style="padding:2px 8px;">
                    <input type="radio" name="letter" id="letter" class="vertical-align: text-bottom;" value="<?php echo $letter->id; ?>" /> <?php echo $letter->title; ?>
                </li>
            <?php } ?>
        </ul>
        <h3><?php echo __('Select group'); ?></h3>
        <?php
        if (empty($groups)) {
            echo __('No groups in system.');
        } else {
            ?>
            <ul style='float:left;clear:both;width:100%;padding:2px 0 15px 0;'>
                <?php foreach ($groups as $group) { ?>
                    <li style="display:block;float:left;padding:2px 8px;">
                        <input type="radio" name="group" value="<?php echo $group->id; ?>" /> <?php echo $group->name; ?>
                    </li>
                <?php } ?>
            </ul>
        <?php } ?>

        <table class="index" border="0" cellpadding="0" cellspacing="0">
            <tr class="node odd">
                <td class="label"><label><?php echo __('Sender'); ?>:</label></td>
                <td><input name="name" type="text"  /></td>
            </tr><tr>
                <td class="label"><label><?php echo __('Sender\'s email address'); ?>:</label></td>
                <td><input name="email" type="text"  /></td>
            </tr><tr>
                <td class="label"><label><?php echo __('Subject'); ?>:</label></td>
                <td><input name="subject" type="text"  /></td>
            </tr>
        </table>
        <?php if (empty($groups)) { ?>
            <div class="box" style="margin:10px 0px; padding: 5px; border:1px solid red;">
                <?php echo __('Unable to send a letter, please create at least one group.');
                ?>
            </div>
        <?php } else { ?>
            <div class="send"><input type="submit" name="get_letter" value="<?php echo __('SEND') ?>"/></div>
            <?php } ?>
    </form>