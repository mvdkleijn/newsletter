== WHAT IT IS ==

Easy Newsletter plugin for Wolf CMS.

Tested against Wolf CMS 0.7.8

Note: this plugin was written as a quick commission and inspired by and lightly
      based on the Simple Newsletter plugin.

== FEATURES ==

    * Add and manage your subscribers in an easy way;
    * Create and modify groups of your subscribers;
    * Add subscribers to one or more groups;
    * Subscribe form anywhere on your web page;
    * Direct unsubscribe link at the end of the letters;
    * Send newsletters to a group;

== HOW TO INSTALL ==

1. Unzip the file and move the newsletter folder to your /wolf/plugins/ directory.
	
2. Go to the Administration > Plugins tab in your Wolf CMS Admin, and activate the plugin.

3. Create a group.

4. Insert the following code where you want your subscription box to appear:
       <?php subscribe("your group name"); ?>
	
5. Add to following basic CSS to your CSS file (or style the form yourself): 

.simple_form {
	width: 260px;
	padding: 4px;
	border: 1px solid #c3c3c3;
	}
.simple_form h4{
	color: #1F3E5F;
	font: normal bold 16px/normal 'Arial', sans-serif;
	padding: 5px;
	clear: both;
	}
.simple_form label {
	width: 80px;
	display: inline-block;
	vertical-align: top;
	clear: both;
	font-weight: bold;
	}
.simple_form input.text {
	width: 160px;
	padding: 2px;
	margin: 0 0 4px 8px;
	border: 1px solid #c3c3c3;
	}
.simple_form input.submit{
	display: block;
	margin-left: 170px;
	width:  auto;
}
div.simple_message{
	color: #FF4500;
	font: normal bold 16px/normal 'Arial', sans-serif;
	padding: 5px;
}


5. That's all. Have fun. In the index.php file you can edit the subscribe form.

== LICENSE ==

Easy Newsletter is licensed under the GPL3.