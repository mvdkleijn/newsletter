<?php

/**
 * Easy Newsletter plugin for Wolf CMS
 * 
 * Based on ideas and code snippets by Laszlo Sebetyen Horvath's Simple Newsletter.
 * 
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2012
 * @license GPL3
 */

// security feature
if (!defined('IN_CMS')) exit;

$driver = Record::getConnection()->getAttribute(PDO::ATTR_DRIVER_NAME);

if ($driver == 'mysql') {
    $sql = 'DROP TABLE IF EXISTS `' . TABLE_PREFIX . 'news_group`';
    if (Record::query($sql) === false) {
        Flash::set('error', __('Unable to delete :tablename table.', array(':tablename' => 'news_group')));
        return;
    }

    $sql = 'DROP TABLE IF EXISTS `' . TABLE_PREFIX . 'news_letter`';
    if (Record::query($sql) === false) {
        Flash::set('error', __('Unable to create :tablename table.', array(':tablename' => 'news_letter')));
        return;
    }

    $sql = 'DROP TABLE IF EXISTS `' . TABLE_PREFIX . 'news_user`';
    if (Record::query($sql) === false) {
        Flash::set('error', __('Unable to create :tablename table.', array(':tablename' => 'news_user')));
        return;
    }

    $sql = 'DROP TABLE IF EXISTS `' . TABLE_PREFIX . 'news_users_groups`';
    if (Record::query($sql) === false) {
        Flash::set('error', __('Unable to create :tablename table.', array(':tablename' => 'news_users_groups')));
        return;
    }
}
