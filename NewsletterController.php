<?php

/**
 * Easy Newsletter plugin for Wolf CMS
 * 
 * Based on ideas and code snippets by Laszlo Sebetyen Horvath's Simple Newsletter.
 * 
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2012
 * @license GPL3
 */
class NewsletterController extends PluginController {

    function __construct() {
        if (defined('CMS_BACKEND') && CMS_BACKEND === true) {
            $this->setLayout('backend');
            $this->assignToLayout('sidebar', new View('../../plugins/newsletter/views/sidebar'));
        }
        else {
            return;
        }
    }

    function secure() {
        AuthUser::load();
        if (!AuthUser::isLoggedIn() && !AuthUser::hasPermission('admin_view'))
            redirect(get_url('login'));
    }

    function index() {
        $this->secure();

        $this->display('newsletter/views/index');
    }

    function users($action = false, $gid = false) {
        $this->secure();
        use_helper('Validate');

        // If adding a user
        if ($action !== false && $action === 'add') {
            $post = $_POST;

            if (empty($post['name']) || empty($post['email'])) {
                Flash::setNow('error', __('Don\'t forget to add a name and email.'));
                $this->users();
            }

            $hash = $post['email'] . $post['name'] . time();
            $hash = md5($hash);
            if (isset($post['group']))
                $groups = $post['group'];
            else
                $groups = array();

            $user = new NewsUser();
            $user->name = $post['name'];
            $user->email = $post['email'];
            $user->hash = $hash;
            $user->subscribed = 1;

            if ($user->save() === false) {
                Flash::setNow('error', __('Unable to add user :username.', array(':username' => $user->name)));
            } else {
                Flash::setNow('success', __('Added user :username successfully.', array(':username' => $user->name)));
            }

            // Add user to groups, if any
            foreach ($groups as $group) {
                $ug = new NewsUserGroups();
                $ug->user_id = $user->id;
                $ug->group_id = $group;
                $ug->save();
            }
        }

        // If deleting a user
        if ($action === 'change' && isset($_POST['user_id']) && isset($_POST['del_users'])) {
            $users = $_POST['user_id'];
            foreach ($users as $user) {
                if (Validate::numeric($user)) {
                    if (NewsUser::deleteWhere('NewsUser', 'id=?', array($user)) !== false) {
                        Flash::setNow('success', __('Successfully deleted one or more users.'));
                    } else {
                        Flash::setNow('error', __('Failed to delete one or more users.'));
                    }
                    
                    NewsUserGroups::deleteWhere('NewsUserGroups', 'user_id=?', array($user));
                }
            }
        }

        // If changing the groups for a user
        if ($action === 'change' && isset($_POST['userid']) && isset($_POST['attach'])) {
            $user = $_POST['userid'];
            if (isset($_POST['group']))
                $groups = $_POST['group'];
            else
                $groups = array('0');

            NewsUserGroups::deleteWhere('NewsUserGroups', 'user_id=?', array($user));
            foreach ($groups as $group) {
                $ug = new NewsUserGroups();
                $ug->user_id = $user;
                $ug->group_id = $group;
                $ug->save();
            }
        }

        // Listing users and their groups
        if ($action === 'list' && $gid !== false && Validate::alpha_numeric($gid)) {
            if ($gid === 'un') {
                $users = NewsUser::findAllFrom('NewsUser', "subscribed=0");
            } else {
                $tmpusers = NewsGroup::findUsers($gid);
                $users = array();
                foreach ($tmpusers as $tmp) {
                    $users = array_merge($users, NewsUser::findAllFrom('NewsUser', "id=? AND subscribed=1", array($tmp->user_id)));
                }
            }
        } else {
            $users = NewsUser::findAllFrom('NewsUser', "subscribed=1");
        }

        $groups = NewsGroup::findAllFrom('NewsGroup');

        $this->display('newsletter/views/users', array('groups' => $groups,
            'users' => $users,
            'groupid' => $gid
        ));
    }

    function groups($action = false) {
        $this->secure();
        use_helper('Validate');

        // If adding a group
        if ($action !== false && $action === 'add' && isset($_POST['name'])) {
            $name = $_POST['name'];
            if (Validate::alphanum_space($name)) {
                $group = new NewsGroup();
                $group->name = $name;
                if ($group->save() === false) {
                    Flash::setNow('error', __('Unable to create group :groupname.', array(':groupname' => $name)));
                } else {
                    Flash::setNow('success', __('Created group :groupname.', array(':groupname' => $name)));
                }
            }
        }

        // If deleting a group
        if ($action !== false && $action === 'delete' && isset($_POST['group'])) {
            $groups = $_POST['group'];
            foreach ($groups as $group) {
                if (Validate::numeric($group)) {
                    if (NewsGroup::deleteWhere('NewsGroup', 'id=?', array($group)) !== false) {
                        Flash::setNow('success', __('Successfully deleted one or more groups.'));
                    } else {
                        Flash::setNow('error', __('Failed to delete one or more groups.'));
                    }
                }
            }
        }

        // Display the screen
        $groups = NewsGroup::findAllFrom('NewsGroup');
        $this->display('newsletter/views/groups', array('groups' => $groups));
    }

    function letters($action = false) {
        $this->secure();

        if ($action === 'add') {
            $letter = new NewsLetter();
            $letter->title = $_POST['title'];
            $letter->subject = $_POST['subject'];
            $letter->content = $_POST['content'];

            if ($letter->save() === false) {
                Flash::setNow('error', __('Unable to create letter :title.', array(':title' => $letter->title)));
            } else {
                Flash::setNow('success', __('Created letter :title successfully.', array(':title' => $letter->title)));
            }
        }

        $letters = NewsLetter::findAllFrom('NewsLetter');

        $this->display('newsletter/views/letters', array('letters' => $letters));
    }

    function letter($action = false) {
        $this->secure();
        use_helper('Validate');

        // Delete a letter
        if ($action === 'change' && isset($_POST['del']) && isset($_POST['id'])) {
            $id = $_POST['id'];
            if (Validate::numeric($id)) {
                if (NewsLetter::deleteWhere('NewsLetter', 'id=?', array($id)) !== false) {
                    Flash::setNow('success', __('Successfully deleted the letter.'));
                } else {
                    Flash::setNow('error', __('Failed to delete the letter.'));
                }
            }

            $this->letters();
        }

        // Modify an existing letter
        if ($action === 'change' && isset($_POST['mod']) && isset($_POST['id'])) {
            $id = $_POST['id'];
            if (Validate::numeric($id)) {
                $letter = NewsLetter::findByIdFrom('NewsLetter', $id);
            } else {
                Flash::setNow('error', __('Invalid letter ID.'));
                $this->letters();
            }

            $this->display('newsletter/views/letter', array('letter' => $letter));
        }

        // Saving after modification
        if ($action === 'change' && isset($_POST['done']) && isset($_POST['id'])) {
            $id = $_POST['id'];
            if (Validate::numeric($id)) {

                $letter = NewsLetter::findByIdFrom('NewsLetter', $id);
                $letter->title = $_POST['title'];
                $letter->subject = $_POST['subject'];
                $letter->content = $_POST['content'];

                if ($letter->save() !== false) {
                    Flash::setNow('success', __('Successfully updated the letter.'));
                } else {
                    Flash::setNow('error', __('Failed to update the letter.'));
                }
            } else {
                Flash::setNow('error', __('Invalid letter ID.'));
                $this->letters();
            }

            $this->display('newsletter/views/letter', array('letter' => $letter));
        }

        $this->display('newsletter/views/letter');
    }

    function documentation() {
        $this->secure();
        $this->display('newsletter/views/index');
    }

    function send($action = false) {
        $this->secure();
        use_helper('Validate');

        // Send the newsletter!
        if ($action === 'confirm') {

            // Check all fields are set
            if (isset($_POST['letter']) && isset($_POST['group']) && isset($_POST['name']) && isset($_POST['email']) && isset($_POST['subject'])) {
                $letterid = $_POST['letter'];
                $groupid = $_POST['group'];
                $fromname = $_POST['name'];
                $frommail = $_POST['email'];
                $fromsubj = $_POST['subject'];
            } else {
                Flash::setNow('error', __('Unable to send newsletter, please complete all fields.'));
                $this->send();
            }

            // Do some sanity checks
            if (!Validate::numeric($letterid) || !Validate::numeric($groupid) || !Validate::email($frommail)) {
                Flash::setNow('error', __('Invalid values submitted, unable to send newsletter.'));
                $this->send();
            }

            $letter = NewsLetter::findByIdFrom('NewsLetter', $letterid);
            $ugs = NewsGroup::findUsers($groupid);
            $users = array();
            foreach ($ugs as $ug) {
                $users[] = NewsUser::findByIdFrom('NewsUser', $ug->user_id);
            }

            $mail = new PHPMailer();
            $body = $letter->content;
            $body = eregi_replace("[\]", '', $body);
            $mail->IsMail();
            $mail->From = $frommail;
            $mail->FromName = $fromname;
            $mail->AddReplyTo($frommail);
            $mail->Subject = $fromsubj;
            $mail->CharSet = "UTF-8";

            foreach ($users as $user) {
                if ($user->subscribed == 0) break;
                
                $hash = $user->hash;
                
                if (defined('USE_MOD_REWRITE') && USE_MOD_REWRITE) {
                    $url = '';
                }
                else {
                    $url = '?/';
                }
                
                $end_body = $body . __('<p>-- <a href=":unsuburl">Unsubscribe</a> --</p>', array(':unsuburl' => URL_PUBLIC.$url.'newsletter/unsubscribe'.URL_SUFFIX.'?id='.$hash));
                $mail->MsgHTML($end_body);
                $mail->AddAddress($user->email, $user->name);
                if (!$mail->Send()) {
                    $error = $mail->ErrorInfo;
                    break;
                }
                $mail->ClearAddresses();
            }

            if (isset($error)) {
                Flash::setNow('error', __('Failed to send newsletter!</br> Error was: :error', array(':error' => $error)));
            } else {
                Flash::setNow('success', __('Newsletter sent successfully!'));
                $this->send();
            }
        }

        $letters = NewsLetter::findAllFrom('NewsLetter');
        $groups = NewsGroup::findAllFrom('NewsGroup');

        $this->display('newsletter/views/send', array('letters' => $letters,
            'groups' => $groups
        ));
    }

    public function unsubscribe() {
        use_helper('Validate');
        
        $id = false;
        if (isset($_GET['id'])) $id = $_GET['id'];

        if ($id !== false) {
            if (Validate::alpha_numeric($id)) {
                $user = NewsUser::findByHash($id);
                $user->subscribed = 0;
                
                if ($user->save()) {
                    echo "<center>" . __("Thank you. You have successfully unsubscribed from the newsletter.") . "</center>";
                }
                else {
                    echo "<center>" . __("Sorry, we were unable to unsubscribe you due to a technical fault. Please try again later.") . "</center>";
                }
            }
        }
        else {
            echo "<center>" . __("Sorry, we were unable to find a matching subscription.") . "</center>";
        }
    }

    public static function subscribe() {
        use_helper('Validate');
        
        if (isset($_POST['name']) && isset($_POST['email'])) {
            
            $name = $_POST['name'];
            $email = $_POST['email'];

            if (!Validate::alphanum_space($name) || !Validate::email($email)) {
                echo "<center>" . __("Error subscribing. Please enter both name and email.") . "</center>";
            } else {
                $hash = $email . $name . time();
                $hash = md5($hash);

                $group = false;

                if (isset($_POST['group']) && Validate::alphanum_space($_POST['group'])) {
                    $group = $_POST['group'];
                    if (NewsGroup::existsIn('NewsGroup', 'name=?', array($group))) {
                        $group = NewsGroup::findOneFrom('NewsGroup', 'name=?', array($group));
                        $group = $group->id;
                    }
                }

                $user = new NewsUser();
                $user->name = $name;
                $user->email = $email;
                $user->hash = $hash;
                $user->subscribed = 1;

                if ($user->save() !== false) {
                    echo "<center>" . __("Thank you for subscribing!") . "</center>";
                } else {
                    echo "<center>" . __("An unknown error occurred while trying to subscribe!") . "</center>";
                }
                
                // Add user to groups, if any
                if ($group !== false) {
                    $ug = new NewsUserGroups();
                    $ug->user_id = $user->id;
                    $ug->group_id = $group;
                    $ug->save();
                }
            }
        }
    }

}