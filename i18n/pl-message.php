<?php

/**
 * Polish translation for plugin Easy Newsletter
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 *
 * Easy Newsletter plugin for Wolf CMS
 *
 * Based on ideas and code snippets by Laszlo Sebetyen Horvath's Simple Newsletter.
 *
 * @author Martijn van der Kleijn <martijn.niji@gmail.com>
 * @copyright Martijn van der Kleijn, 2012
 * @license GPL3
 */

return array(
    '<p>-- <a href=":unsuburl">Zakończ subskrybcję</a> --</p>' => '<p>-- <a href=":unsuburl">Zakończ subskrybcję</a> --</p>',
    'Add a group' => 'Dodaj grupę',
    'Add a new letter' => 'Dodaj nowy list',
    'Add a new subscriber' => 'Dodaj nowego subskrybenta',
    'Add' => 'Dodaj',
    'Added user :username successfully.' => 'Pomyślnie dodano użytkownika :username.',
    'An unknown error occurred while trying to subscribe!' => 'Wystąpił nieznany błąd przy próbie subskrybcji!',
    'Change groups' => 'Zmień grupy',
    'Content' => 'Zawartość',
    'Created group :groupname.' => 'Utworzono grupę :groupname.',
    'Created letter :title successfully.' => 'Pomyślnie utworzono list :title.',
    'Delete one or more groups' => 'Usuń jedną lub więcej grup',
    'Delete' => 'Usuń',
    'Don\'t forget to add a name and email.' => 'Nie zapomnij podać imienia i adresu email.',
    'Error subscribing. Please enter both name and email.' => 'Błąd subskrybowania. Podaj imię i email.',
    'Existing letters' => 'Istniejące listy',
    'Failed to delete one or more groups.' => 'Nie udało się usunąć jednej lub więcej grup.',
    'Failed to delete one or more users.' => 'Nie udało się usunąć jednego lub więcej użytkowników.',
    'Failed to delete the letter.' => 'Nie udało się usunąć listu.',
    'Failed to send newsletter!</br> Error was: :error' => 'Nie udało się wysłać newslettera!</br> Błąd: :error',
    'Failed to update the letter.' => 'Nie udało się usunąć zaktualizować listu.',
    'Group Management' => 'Zarządzanie Grupami',
    'Groups and their members' => 'Grupy i ich członkowie',
    'Groups' => 'Grupy',
    'How to use it?' => 'Jak tego używać?',
    'Insert the following code where you want your subscription box to appear:' => 'Wstaw poniższy kod w miejscu gdzie ma się znajdować okienko subskrybcji:',
    'Invalid letter ID.' => 'Niewłaściwy ID listu.',
    'Invalid values submitted, unable to send newsletter.' => 'Wprowadzono błędne wartości, Nie udało się wysłać newslettera.',
    'Letter Management' => 'Zarządzanie Listami',
    'Modify' => 'Modyfikuj',
    'New group name' => 'Nazwa nowej grupy',
    'Newsletter sent successfully!' => 'Newsletter wysłany pomyślnie!',
    'Newsletter subscription' => 'Subskrybcja newslettera',
    'Newsletter' => 'Newsletter',
    'No groups in system.' => 'Brak grup w systemie.',
    'Our apologies, the newsletter system is not yet available.' => 'Przepraszamy, system newsletterów jest chwilowo niedostępny.',
    'SEND' => 'Wyślij',
    'Save' => 'Zapisz',
    'Select group' => 'Wybierz grupę',
    'Select letter' => 'Wybierz list',
    'Send Newsletter' => 'Wyślij Newsletter',
    'Send letter' => 'Wyślij list',
    'Sender' => 'Nadawca',
    'Sender\'s email address' => 'Adres email nadawcy',
    'Sorry, we were unable to find a matching subscription.' => 'Przepraszamy, nie znaleziono pasującej subskrybcji.',
    'Sorry, we were unable to unsubscribe you due to a technical fault. Please try again later.' => 'Przepraszamy, z przyczyn technicznych nie udało się Ciebie wypisać. Spróbuj później.',
    'Subject' => 'Temat',
    'Subscribe' => 'Subskrybuj',
    'Subscribed' => 'Subskrybujący',
    'Subscribers' => 'Subskrybenci',
    'Successfully deleted one or more groups.' => 'Pomyślnie usunięto jedną lub więcej grup.',
    'Successfully deleted one or more users.' => 'Pomyślnie usunięto jednego lub więcej użytkowników.',
    'Successfully deleted the letter.' => 'Pomyślnie usunięto list.',
    'Successfully updated the letter.' => 'Pomyślnie zaktualizowano list.',
    'Thank you for subscribing!' => 'Dziękujemy za subskrybcję!',
    'Thank you. You have successfully unsubscribed from the newsletter.' => 'Dziękujemy. Zakończyłeś subskrybcję newslettera.',
    'Title' => 'Tytuł',
    'Unable to add user :username.' => 'Nie można dodać użytkownika :username.',
    'Unable to create group :groupname.' => 'Nie można utworzyć grupy :groupname.',
    'Unable to create letter :title.' => 'Nie można utworzyć listu :title.',
    'Unable to send a letter, please create at least one group.' => 'Nie można wysłać listu, utwórz przynajmniej jedną grupę.',
    'Unable to send newsletter, please complete all fields.' => 'Nie można wysłać listu, wypełnij wszystkie pola.',
    'Unsubscribed' => 'Niesubskrybujący',
    'You didn\'t fill out the "Email" field' => 'Nie wypełniłeś pola "Email"',
);